<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/thank-you', 'HomeController@thankyou');

Auth::routes();
Auth::routes(['register' => false, 'verify' => true]);
Route::get('/log-in', 'HomeController@log_in');
Route::get('/admin', 'HomeController@log_in');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/product/{product_id}', 'HomeController@view_product')->name('view_product');

Route::group(['middleware' =>'admin_auth','prefix' => 'admin'], function(){
    Route::get('/dashboard', 'AdminController@index');
    Route::get('/credit', 'AdminController@credit');
    Route::get('/dues', 'BillController@dues');
    Route::post('/credit/add', 'CreditController@add_credit')->name('add_credit');
    Route::post('/credit/cancel', 'CreditController@cancel_credit')->name('cancel_credit');
    Route::get('/credit/view/{credit_id}', 'CreditController@view_credit')->name('view_credit');
    Route::get('/credit/all', 'AdminController@credit_all')->name('credit_all'); 
    Route::get('/remittance', 'RemittanceController@remittance');
    Route::get('/remittance/add', 'RemittanceController@add_remittance')->name('add_remittance');
    


    Route::get('/cash', 'CashController@cash');
    Route::get('/cash/view/{cash_id}', 'CashController@cash_view');
    Route::post('/cash/add', 'CashController@add_cash')->name('add_cash');
    
    Route::get('/accounts', 'AdminController@accounts');
    Route::post('/account/add', 'AccountController@add_account')->name('add_account');
    Route::get('/account/{account_id}', 'AccountController@view_account')->name('view_account');

    Route::post('/bill/pay', 'BillController@bill_pay')->name('bill_pay');
    Route::get('/account/bill/history/{account_id}', 'AccountController@view_account_bill_history');
    Route::get('/account/payment/history/{account_id}', 'AccountController@view_account_payment_history');
    Route::get('/account_search', 'AccountController@account_search');
    Route::post('/account/edit', 'AccountController@edit_account')->name('edit_account');
    
    Route::get('/payments', 'AdminController@payments');
    Route::post('/payments/filter', 'AdminController@filter_payments')->name('filter_payments');
    Route::post('/payment/cancel', 'CreditController@cancel_payment')->name('cancel_payment');
    
    Route::get('/items', 'AdminController@items');
    Route::post('/item/add', 'ItemController@add_item')->name('add_item');
    Route::post('/item/add/picture', 'ItemController@add_product_image')->name('add_product_image');
    Route::post('/item/edit', 'ItemController@edit_item')->name('edit_item');
    Route::get('/item/view/{item_id}', 'ItemController@view_item')->name('view_item');
    Route::get('/item/active/{item_id}', 'ItemController@item_active')->name('item_active');
    Route::get('/item/inactive/{item_id}', 'ItemController@item_inactive')->name('item_inactive');
    
    Route::get('/stocks', 'StockController@stocks');
    Route::post('/stock/add', 'StockController@add_stock')->name('add_stock');
    Route::get('/stock/add_stock/{stock_id}', 'StockController@add_stocks_record');
    Route::post('/stock/add_stock_record', 'StockController@add_stocks_record_detail');
    Route::post('/stock/update_stock_record', 'StockController@update_stock_record');
    Route::get('/stock/remove_stock/{stock_details_id}', 'StockController@remove_stocks_record');
    Route::get('/stock/save/{batch_id}', 'StockController@save_stock');
    Route::get('/stock/receive/{batch_id}', 'StockController@receive_stock');
    Route::get('/stock/view/{batch_id}', 'StockController@view_stock');

   
    
    Route::get('/reports', 'AdminController@reports');
    Route::get('/settings', 'AdminController@settings');

    Route::get('/brands', 'BrandController@read_brand')->name('read_brand');
    Route::post('/brands/addbrands', 'BrandController@add_brand')->name('add_brand');
    Route::post('/brands/editbrands', 'BrandController@edit_brand')->name('edit_brand');
    Route::post('/brands/deletebrands', 'BrandController@delete_brand')->name('delete_brand');

    Route::get('/users', 'AdminController@users');
    Route::post('/user/add', 'UserController@add_user')->name('add_user');
    Route::post('/user/edit', 'UserController@edit_user');
});

Route::group(['middleware' =>'collector_auth','prefix' => 'collector'], function(){

    Route::get('/dashboard', 'CollectorController@index');
    Route::get('/credit', 'CollectorController@credit');
    Route::get('/dues', 'BillController@dues_collector');
    Route::get('/credit/view/{credit_id}', 'CreditController@view_credit_controller')->name('view_credit_controller');
    Route::get('/credit/all', 'CollectorController@credit_all')->name('credit_all');
    
    Route::post('/bill/pay', 'BillController@bill_pay_controller')->name('bill_pay_controller');
    Route::get('/account/bill/history/{account_id}', 'AccountController@view_account_bill_history_controller');
    Route::get('/account/payment/history/{account_id}', 'AccountController@view_account_payment_history_controller');
    Route::get('/account_search', 'AccountController@account_search_collector');
    Route::get('/account/{account_id}', 'AccountController@view_account_controller')->name('view_account_controller');
    Route::get('/accounts', 'CollectorController@accounts_controller');
    
    
    Route::get('/payments', 'CollectorController@payments');
    Route::get('/items', 'CollectorController@items');
    Route::get('/item/view/{item_id}', 'ItemController@view_item_controller')->name('view_item_controller');
   
    Route::get('/stocks', 'StockController@stocks_controller');
    
    Route::post('/stock/update_stock_record', 'StockController@update_stock_record_collector');
    Route::get('/stock/save/{batch_id}', 'StockController@save_stock');
    Route::get('/stock/receive/{batch_id}', 'StockController@receive_stock_collector');
    Route::get('/stock/view/{batch_id}', 'StockController@view_stock_collector');

    Route::post('/user/edit', 'UserController@edit_user');
});


Route::group(['middleware' =>'account_auth','prefix' => 'account'], function(){

    Route::get('/dashboard', 'AccountController@account');
    Route::get('/bill/history/{account_id}', 'AccountController@bill_history');
    Route::get('/payment/history/{account_id}', 'AccountController@payment_history');
    Route::get('/items', 'AccountController@items');
    Route::post('/user/edit', 'UserController@edit_user');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
