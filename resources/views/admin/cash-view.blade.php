@extends('layout.admin')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          
          
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
               
                  <h1 class=" text-dark text-center">Quians Cellshop Bill Invoice</h1>
                
                <div class="card-body">
                      
                  <div class="invoice p-3 mb-3">
              <!-- title row -->
             
              <!-- info row -->
              <div class="row invoice-info">
               
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  To
                  <address>
                    <strong>{{strtoupper($data_cash->name)}}</strong><br>
                  
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  Item details:
                  <br>
                  <b>{{$data_cash->product->product_name}} - {{$data_cash->product->model}}</b><br>
                  <b>Amount:</b> {{$data_cash->amount}}<br> 
                  <b>Quantity:</b> {{$data_cash->quantity}}<br> 
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
            

              
              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-lg-12">
                <button class="btn btn-info btn-sm no-print" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                <a class="btn btn-default btn-sm no-print" href="/admin/dashboard"><i class="fa fa-reply"></i> Back</a>
                  
                </div>
              </div>
            </div>
                        
                    </div>
                    
                </div>

            </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    
@endsection