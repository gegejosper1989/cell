@extends('layout.admin')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Stocks</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
              <li class="breadcrumb-item active">Stocks</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-5">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"> Item List</h3>
                    </div>
                    <div class="card-body">
                    <table class="table table-striped" id="table">
                            <thead>
                            <tr>
                                <th> Item </th>             
                                <th> Model </th>
                                <th> </th>
                            </tr>
                            </thead>
                            <tbody class="itemresult"> 
                                @foreach($data_item as $Item)
                                    <tr>
                                        <td>{{$Item->product_name}}</td>
                                        <td>{{$Item->model}}</td>
                                        <td>
                                            <a href="javascript:;" class="btn btn-success btn-sm no-print addstockmodal"
                                            data-batch_id="{{$data_stock->batch_id}}"
                                            data-product_id="{{$Item->id}}"
                                            data-product_name="{{$Item->product_name}}" 
                                            data-model="{{$Item->model}}"  
                                            >
                                            <i class="fas fa-plus"></i>
                                            </a>  
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                    </table>
                            
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Item Record List</h3>
                    </div>
                    <div class="card-body table-responsive">
                        @if(Session::has('danger'))
                          <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-check"></i> Success!</h5>
                            {{ Session::get('danger') }}
                              @php
                              Session::forget('danger');
                              @endphp
                          </div>
                        @endif
                        <table class="table table-striped" id="table">
                            <thead>
                            <tr>
                                <th> Item </th>             
                                <th> Quantity</th>
                                <th> </th>
                            </tr>
                            </thead>
                            <tbody class="productresult">
                            @foreach($data_stock_detail as $stock_detail)
                            <tr>
                                <td>
                                    <a href="/admin/stock/view/{{$stock_detail->product->id}}">{{$stock_detail->product->product_name}} - {{$stock_detail->product->model}}</a>
                                </td>
                                <td>
                                    {{$stock_detail->quantity}}
                                </td>
                                <td>
                                    <a href="/admin/stock/remove_stock/{{$stock_detail->id}}" class="btn btn-danger btn-sm">
                                        <i class="fas fa-times"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-12 text-center">
                        <div class="form-group">
                            <a href="/admin/stock/save/{{$data_stock->batch_id}}" class="btn btn-info text-center"> <i class=" fas fa-save"></i> SAVE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
<div class="modal fade" id="modalstock" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Add Stock</h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">

                <div class="col-lg-4">
                    <div class="form-group">
                    <label for="">Item</label>
                        @csrf
                        <input type="text" name="item" id="item" class="form-control" readonly>
                        <input type="hidden" name="product_id" id="product_id" class="form-control">
                        <input type="hidden" name="batch_id" id="batch_id" class="form-control">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                    <label for="">Model </label>
                        <input type="text" name="model" id="model" class="form-control"  readonly>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                    <label for="">Quantity</label>
                        <input type="number" name="quantity" id="quantity" class="form-control" placeholder="0" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-success addtolist">Add</button>
            </div>
        </div>
        </div>
    </div>
    </div>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/stockrecordscript.js') }}"></script>    
@endsection