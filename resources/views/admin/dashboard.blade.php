@extends('layout.admin')

@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                <div class="inner">
                    <h3>{{count($data_account)}}</h3>

                    <p>Accounts</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="/admin/accounts" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{$data_payment}}</h3>

                <p>Total Payment</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="/admin/payments" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{number_format($collectibles,2)}}</h3>

                <p>Total Collectibles</p>
              </div>
              <div class="icon">
                <i class="fas fa-money-bill"></i>
              </div>
              <a href="/admin/credit" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>{{count($data_bill_due)}}</h3>

                <p>Overdues</p>
              </div>
              <div class="icon">
                <i class="fas fa-money-bill-alt"></i>
              </div>
              <a href="/admin/dues" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
            <div class="card ">
            <div class="card-header">
                <h3 class="card-title">Add Credit</h3>
                <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
            </div>
          <!-- /.card-header -->
          <div class="card-body" style="display: block;">        
            <form enctype="multipart/form-data" class="" action="{{route('add_credit')}}" method="post">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="">Account </label>
                                <select name="account" id="account" class="form-control" required>
                                <option></option>
                                    @foreach($data_account as $Account)
                                        <option value="{{$Account->id}}">{{strtoupper($Account->lname)}}, {{strtoupper($Account->fname)}}</option>
                                    @endforeach
                                </select>
                                
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="">Item </label>
                                <select name="item" id="item" class="form-control" required>
                                    <option></option>
                                    @foreach($data_item as $Item)
                                        <option value="{{$Item->id}}, {{$Item->unit_price}}">{{$Item->product_name}} - {{$Item->model}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" name="product_id" id="product_id" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                            <label for="">Amount</label>
                                <input type="text" name="amount" id="amount" class="form-control" placeholder="Amount" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                            <label for="">Downpayment</label>
                                <input type="text" name="downpayment" id="downpayment" class="form-control" placeholder="Downpayment" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                            <label for="">Term</label>
                                <input type="number" name="term" id="term" class="form-control" placeholder="Term" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                            <label for="">Quantity</label>
                                <input type="number" name="quantity" id="quantity" class="form-control" placeholder="Quantity" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                            <label for="">Date</label>
                                <input type="date" name="date_credit" id="date_credit" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                            <button type="submit" class="btn btn-info text-center"> <i class=" fas fa-save"></i> Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>  
          </div>
          <!-- /.card-body -->
          
        </div>
        <div class="row">
            <div class="col-lg-12">
            <div class="card ">
            <div class="card-header">
                <h3 class="card-title">Cash</h3>
                <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
            </div>
          <!-- /.card-header -->
          <div class="card-body" style="display: block;">        
            <form class="" action="{{route('add_cash')}}" method="post">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="">Full Name </label>
                                <input type="text" name="name" id="name" class="form-control" placeholder="Full Name" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="">Item </label>
                                <select name="cashitem" id="cashitem" class="form-control" required>
                                    <option></option>
                                    @foreach($data_item as $Item)
                                        <option value="{{$Item->id}}, {{$Item->cash_price}}">{{$Item->product_name}} - {{$Item->model}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" name="cash_product_id" id="cash_product_id" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                            <label for="">Amount</label>
                                <input type="text" name="cash_amount" id="cash_amount" class="form-control" placeholder="Amount" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                            <label for="">Quantity</label>
                                <input type="number" name="cash_quantity" id="cash_quantity" class="form-control" placeholder="Quantity" required>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                            <button type="submit" class="btn btn-info text-center"> <i class=" fas fa-save"></i> Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>  
          </div>
          <!-- /.card-body -->
          
        </div>
      
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
<script>
$(document).ready(function(){
    $("#item").change(function () {
    var item = $('select[name=item]').val();
        var array = item.split(',');
        var product_id = array[0];
        var amount = array[1]; 
    $('#product_id').val(product_id);
    $('#amount').val(amount);
    });

    $("#cashitem").change(function () {
    var item = $('select[name=cashitem]').val();
        var array = item.split(',');
        var product_id = array[0];
        var amount = array[1]; 
    $('#cash_product_id').val(product_id);
    $('#cash_amount').val(amount);
    });
});
</script>
<script type="text/javascript">

$("#account").select2({
  tags: true
});


</script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/creditscript.js') }}"></script>
<script src="{{ asset('js/cashscript.js') }}"></script>
@endsection