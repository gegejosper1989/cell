@extends('layout.admin')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Credit</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
              <li class="breadcrumb-item active">Credit</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <!-- /.row (main row) -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Credit List</h3>
                    </div>
                    <div class="card-body" style="display: block;">
                    
                        <table class="table table-striped" id="table">
                            <thead>
                            <tr>
                                <th> Name</th>
                                <th> Item </th>             
                                <th> Amount</th>
                                <th> Downpayment</th>
                                <th> Balance </th>
                                <th> Term </th>
                                <th> Term Payment</th>
                                <th> Status</th>
                            </tr>
                            </thead>
                            <tbody class="creditresult">
                                @foreach($data_credit as $Credit)
                                <tr>
                                    <td><a href="/admin/account/{{$Credit->account->id}}">{{$Credit->account->lname}}, {{$Credit->account->fname}}</a></td>
                                    <td>{{$Credit->product->product_name}}</td>
                                    <td>{{$Credit->amount}}</td>
                                    <td>{{$Credit->downpayment}}</td>
                                    <td>{{$Credit->balance}}</td>
                                    <td>{{$Credit->term}}</td>
                                    <td>{{$Credit->term_payment}}</td>
                                    <td>{{$Credit->status}}</td>
                                    <td>
                                    <a href="/admin/account/{{$Credit->account->id}}" class="btn btn-success btn-sm no-print">
                                    <i class="fas fa-money-bill-alt"> Pay</i>
                                    </a> 
                                    
                                    @if(count($Credit->payment_record) == 0)
                                    <a href="javascript:;" class="btn btn-danger btn-sm no-print cancelmodal"
                                        data-credit_id="{{$Credit->id}}">
                                    <i class="fas fa-times"> Cancel</i>
                                    </a>  
                                    @endif

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <div class="modal fade" id="modalcancel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Cancel Credit</h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">

                Are you sure you want to cancel this Credit?
                <input type="hidden" name="credit_id" id="credit_id">
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-danger cancelcredit">Process Cancel</button>
            </div>
        </div>
        </div>
    </div>
    </div>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/creditscript.js') }}"></script>
<script>
$(document).ready(function(){
    $("#item").change(function () {
    var item = $('select[name=item]').val();
        var array = item.split(',');
        var product_id = array[0];
        var amount = array[1]; 
    $('#product_id').val(product_id);
    $('#amount').val(amount);
    });
});
</script>
<script type="text/javascript">

$("#account").select2({
  tags: true
});


</script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/creditscript.js') }}"></script>
@endsection