@extends('layout.admin')

@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Remittance</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
              <li class="breadcrumb-item active">Remittance</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Remittance History</h3>
                    </div>
                
                    <div class="card-body table-responsive">
                        @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h5><i class="icon fas fa-check"></i> Success!</h5>
                                {{ Session::get('success') }}
                                @php
                                Session::forget('success');
                                @endphp
                            </div>
                        @endif
                        {{$data_remittance->links()}}
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                <th>Date</th>    
                                <th>Inclusions Date</th>
                                <th>Amount</th>
                                
                                </tr>
                            </thead>
                            <tbody>

                                @forelse($data_remittance as $Remittance)
                                <tr>
                                    <td>{{$Remittance->remittance_date}}</td>
                                    <td>{{$Remittance->inclusion_date}}</td>
                                    <td>{{$Remittance->amount_remittance}}</td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="3"><em>No Record</em></td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                        <button class="btn btn-info btn-sm no-print" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                     
                    </div>
                </div>
            </div>
            
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
@endsection