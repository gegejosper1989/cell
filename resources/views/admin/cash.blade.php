@extends('layout.admin')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Cash</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
              <li class="breadcrumb-item active">Cash</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12">
            <div class="card ">
            <div class="card-header">
                <h3 class="card-title">Cash</h3>
                <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
            </div>
          <!-- /.card-header -->
          <div class="card-body" style="display: block;">        
            <form class="" action="{{route('add_cash')}}" method="post">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="">Full Name </label>
                                <input type="text" name="name" id="name" class="form-control" placeholder="Full Name" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="">Item </label>
                                <select name="cashitem" id="cashitem" class="form-control" required>
                                    <option></option>
                                    @foreach($data_item as $Item)
                                        <option value="{{$Item->id}}, {{$Item->cash_price}}">{{$Item->product_name}} - {{$Item->model}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" name="cash_product_id" id="cash_product_id" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                            <label for="">Amount</label>
                                <input type="text" name="cash_amount" id="cash_amount" class="form-control" placeholder="Amount" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                            <label for="">Quantity</label>
                                <input type="number" name="cash_quantity" id="cash_quantity" class="form-control" placeholder="Quantity" required>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                            <button type="submit" class="btn btn-info text-center"> <i class=" fas fa-save"></i> Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>  
          </div>
          <!-- /.card-body -->
          
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Cash List</h3>
                        
                    </div>
                    <div class="card-body" style="display: block;">
                        {{$data_cash->links()}} 
                       
                        <table class="table table-striped" id="table">
                            <thead>
                            <tr>
                                <th> Name</th>
                                <th> Item </th>             
                                <th> Amount</th>
                                <th> Quantity</th>
                            </tr>
                            </thead>
                            <tbody class="creditresult">
                                @foreach($data_cash as $Cash)
                                <tr>
                                    <td>{{$Cash->name}}</td>
                                    <td>{{$Cash->product->product_name}} {{$Cash->product->model}}</td>
                                    <td>{{$Cash->amount}}</td>
                                    <td>{{$Cash->quantity}}</td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <script>
$(document).ready(function(){
    $("#cashitem").change(function () {
    var item = $('select[name=cashitem]').val();
        var array = item.split(',');
        var product_id = array[0];
        var amount = array[1]; 
    $('#cash_product_id').val(product_id);
    $('#cash_amount').val(amount);
    });
});
</script>
@endsection