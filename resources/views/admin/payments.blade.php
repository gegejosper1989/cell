@extends('layout.admin')

@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Payments</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
              <li class="breadcrumb-item active">Payments</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Payment History</h3>
                    </div>
                    <div class="no-print">
                    <form action="{{route('filter_payments')}}" method="post">
                        @csrf
                        <div class="row card-body">
                            
                            <div class="col-md-2">
                                <div class="form-group">
                                <label>From:</label>

                                    <div class="input-group">
                                    
                                    <input type="date" style="width: 150px" name="fromdate" id="fromdate" class="form-control" required>
                                    </div>
                                <!-- /.input group -->
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                <label>To:</label>
                                    <input type="date" style="width: 150px" name="todate" id="todate" class="form-control" required>
                                </div>
                                <!-- /.input group -->
                               
                            </div>
                            <div class="col-md-2">
                                <div class="form-group" style="padding-top:25px;">
                                    <button type="submit" class="form-control btn btn-info btn-sm">Filter</button>
                                </div>
                            </div>
   
                        </div>
                        </form>
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                <th>Date</th>    
                                <th>Bill No.</th>
                                <th>Account</th>
                                <th>Item</th>
                                <th>Amount</th>
                                <th>Posted by</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $total_amount = 0; ?>
                                @forelse($data_payment as $Payment)
                                <tr>
                                    <td>{{$Payment->payment_date}}</td>
                                    <td>{{$Payment->bill_id}}</td>
                                    <td><a href="/admin/account/{{$Payment->account->id}}">{{strtoupper($Payment->account->lname)}}, {{strtoupper($Payment->account->fname)}} {{strtoupper($Payment->account->mname)}}</a></td>
                                    <td>{{$Payment->bill->credit->product->product_name}} {{$Payment->bill->credit->product->model}}</td>
                                    <td>{{$Payment->amount}}</td>
                                    <td>{{$Payment->user->name}}</td>
                                    <?php $total_amount = $total_amount + $Payment->amount;?>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="5"><em>No Record</em></td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                        <div class="row" style="margin-top:20px;">
                            <div class="col-6">
                                
                            </div>
                            <div class="col-6">
                                <form action="{{route('add_remittance')}}" method="action">
                                @csrf
                                    <div class="table-responsive">
                                        <table class="table">
                                        <tbody>
                                        <tr>
                                            <th style="width:50%">Date:</th>
                                            
                                            <td><input type="text" name="inclusion_date" class="form-control" value="{{date('d/m/Y',strtotime($from_date))}} - {{date('d/m/Y',strtotime($to_date))}}" readonly></td>
                                        </tr>
                                        <tr>
                                            <th style="width:50%">Total Amount:</th>
                                            
                                            <td> <input type="text" class="form-control" name="amount_remittance" value="{{number_format($total_amount,2)}}"></td>
                                        </tr>

                                        <tr>
                                            <th style="width:50%"></th>
                                            
                                            <td class="text-right"> <button type="submit" class="btn btn-success"> Save</button></td>
                                        </tr>
                                        </tbody></table>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <button class="btn btn-info btn-sm no-print" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                     
                    </div>
                </div>
            </div>
            
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
@endsection