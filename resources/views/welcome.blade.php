@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h3 class="text-center">Welcome Quians Cellshop</h3>
            <div class="card">
            
                <div class="row">
                    
                    <div class="col-12">
                    <table class="table table-striped" id="table">
                        <thead>
                        <tr>
                            <th></th>
                            <th> Item </th>             
                            <th> Brand</th>
                            <th> Model</th>
                            <th> Quantity </th>
                            <th> Installment Price </th>
                            <th> Cash Price </th>
                            <th> Availability </th>
                        </tr>
                        </thead>
                        <tbody class="productresult">
                        @foreach($data_item as $Item)
                        <tr>
                            
                            <td>
                            <?php $count = 0; ?>
                            @foreach($Item->pic as $Pic)
                            <?php 
                                if($count < 1){
                            ?>
                            <a href="/product/{{$Item->id}}" ><img width="50" src="{{asset('productimg')}}/{{$Pic->file_name}}"></a></td>
                            <?php
                                }
                                $count++;
                            ?>
                            @endforeach
                            <td>
                            <a href="/product/{{$Item->id}}" >{{$Item->product_name}}</a>
                            </td>
                            <td>
                                {{$Item->branddetails->brand_name}}
                            </td>
                            <td>
                                {{$Item->model}}    
                            </td>
                            <td>
                                {{$Item->quantity}}    
                            </td>
                            <td>
                                {{$Item->unit_price}}   
                            </td>
                            <td>
                                {{$Item->cash_price}}  
                            </td>
                            <td>
                            @if($Item->status == 'inactive')
                                <em>Not Available</em>
                                @else
                                <em>Available</em>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{$data_item->links()}}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection