@extends('layout.account')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Items</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/collector/dashboard">Home</a></li>
              <li class="breadcrumb-item active">Items</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Item List</h3>
                    </div>
                    <div class="card-body table-responsive">
                        @if(Session::has('success'))
                          <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-check"></i> Success!</h5>
                            {{ Session::get('success') }}
                              @php
                              Session::forget('success');
                              @endphp
                          </div>
                        @endif
                        <table class="table table-striped" id="table">
                            <thead>
                            <tr>
                                <th></th>
                                <th> Item </th>             
                                <th> Brand</th>
                                <th> Model</th>
                                <th> Quantity </th>
                                <th> Installment Price </th>
                                <th> Cash Price </th>
                                <th> Availability </th>
                            </tr>
                            </thead>
                            <tbody class="productresult">
                            @foreach($data_item as $Item)
                            <tr>
                                
                                <td>
                                <?php $count = 0; ?>
                                @foreach($Item->pic as $Pic)
                                <?php 
                                    if($count < 1){
                                ?>
                                <a href="/collector/item/view/{{$Item->id}}" ><img width="50" src="{{asset('productimg')}}/{{$Pic->file_name}}"></a></td>
                                <?php
                                    }
                                    $count++;
                                ?>
                                @endforeach
                                <td>
                                    {{$Item->product_name}}
                                </td>
                                <td>
                                    {{$Item->branddetails->brand_name}}
                                </td>
                                <td>
                                    {{$Item->model}}    
                                </td>
                                <td>
                                    {{$Item->quantity}}    
                                </td>
                                <td>
                                    {{$Item->unit_price}}   
                                </td>
                                <td>
                                    {{$Item->cash_price}}  
                                </td>
                                <td>
                                @if($Item->status == 'inactive')
                                    <em>Not Available</em>
                                    @else
                                    <em>Available</em>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{$data_item->links()}}
                </div>
            </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>

    <script>
		$(document).ready(function () {
	            $.ajaxSetup({
	                headers: {
	                    'X-CSRF-TOKEN': '{{csrf_token()}}'
	                }
	            });

	            $('.xedit').editable({
	                url: '{{url("/collector/item/edit")}}',
	                title: 'Update',
	                success: function (response, newValue) {
	                    console.log('Updated', response)
	                }
                });
        })
    </script>

    
@endsection