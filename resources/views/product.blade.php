@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            <div class="row">
                <div class="col-12 col-sm-6">
                
                <div class="col-12">
                <?php $count = 0; ?>
                    @foreach($data_item->pic as $Pic)
                        <?php 
                            if($count < 1){
                        ?>
                        <img src="{{asset('productimg')}}/{{$Pic->file_name}}" class="product-image" style="width:250px;">
                        <?php
                            
                            }
                            
                            $count++;
                        ?>
                    @endforeach
                
                </div>
                <div class="col-12 product-image-thumbs">
                    @foreach($data_picture as $Picture)
                        <div class="product-image-thumb">
                            <img src="{{asset('productimg')}}/{{$Picture->file_name}}" alt="{{$data_item->product_name}}" style="width:50px;">
                        </div>
                    @endforeach
                </div>
                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-check"></i> Success!</h5>
                        {{ Session::get('success') }}
                            @php
                            Session::forget('success');
                            @endphp
                        </div>
                    @endif
                </div>
                
                <div class="col-12 col-sm-6">
                    <h3 class="my-3"> 
                        <a href="/product/{{$data_item->id}}">{{$data_item->product_name}}</a>
                    </h3>
                    <hr>
                
                    <ul class="list-group list-group-unbordered mb-3">
                    <li class="list-group-item">
                        <b>Brand : </b> 
 
                            {{$data_item->branddetails->brand_name}}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Model : </b> 

                                {{$data_item->model}}    
                    
                    </li>
                    <li class="list-group-item">
                        <b>Quantity : </b> 

                                    {{$data_item->quantity}}    
                        
                    </li>
                    <li class="list-group-item">
                        <b>Installment Amount : </b> 

                                {{$data_item->unit_price}}   
                   
                    </li>
                    <li class="list-group-item">
                        <b>Cash Amount : </b> 
  
                                {{$data_item->cash_price}}   
                        
                    </li>
                    </ul>

                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection