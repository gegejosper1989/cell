@extends('layout.collector')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Stocks Record Detail</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/collector/dashboard">Home</a></li>
              <li class="breadcrumb-item active">Stocks</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body table-responsive">
                        @if(Session::has('success'))
                          <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-check"></i> Success!</h5>
                            {{ Session::get('success') }}
                              @php
                              Session::forget('success');
                              @endphp
                          </div>
                        @endif
                        <div class="stockdetails">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3">
                                            <div class="form-group">
                                            <label for="">Batch Number </label> <br />
                                            {{$data_stock->batch_id}}
                                            
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="form-group">
                                            <label for="">Sending Date</label>
                                            <br>
                                            {{$data_stock->date_send}}
                                    
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="form-group">
                                            <label for="">Recieve Date</label> <br >
                                            {{$data_stock->date_receive}}
                                               
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="form-group">
                                            <label for="">Status</label> <br >
                                            {{$data_stock->status}}
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                        </div>  
                        <table class="table table-striped" id="table">
                            <thead>
                            <tr>
                                <th> Item </th>             
                                <th> Quantity</th>
                                <th> Receive Quantity</th>
                                <th> </th>
                            </tr>
                            </thead>
                            <tbody class="productresult">
                            @foreach($data_stock_detail as $stock_detail)
                            <tr>
                                <td>
                                    {{$stock_detail->product->product_name}} - {{$stock_detail->product->model}}
                                </td>
                                <td>
                                    {{$stock_detail->quantity}}
                                </td>
                                <td>
                                    {{$stock_detail->quantity_receive}}
                                </td>
                                <td>
                                    @if($stock_detail->status != 'RECEIVED')
                                    <a href="javascript:;" class="btn btn-success btn-sm no-print addstockmodal"
                                            data-product_name="{{$stock_detail->product->product_name}}" 
                                            data-product_id="{{$stock_detail->product->id}}" 
                                            data-stock_detail_id="{{$stock_detail->id}}" 
                                            data-model="{{$stock_detail->product->model}}"  
                                            >
                                    <i class="fas fa-plus"></i>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-12 text-center no-print">
                        <div class="col-lg-12">
                            <button class="btn btn-info no-print" onclick="window.print();"><i class="fa fa-print"></i> PRINT</button>
                            @if($data_stock->status != 'RECEIVED')
                            <a href="/collector/stock/receive/{{$data_stock->batch_id}}" class="btn btn-success text-center"> <i class=" fas fa-save"></i> RECIEVE</a>
                            @endif
                        </div>
                    <br />
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
<div class="modal fade" id="modalstock" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Add Stock</h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">

                <div class="col-lg-4">
                    <div class="form-group">
                    <label for="">Item</label>
                        @csrf
                        <input type="text" name="item" id="item" class="form-control" readonly>
                        <input type="hidden" name="stock_detail_id" id="stock_detail_id" class="form-control">
                        <input type="hidden" name="product_id" id="product_id" class="form-control">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                    <label for="">Model </label>
                        <input type="text" name="model" id="model" class="form-control"  readonly>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                    <label for="">Quantity</label>
                        <input type="number" name="quantity" id="quantity" class="form-control" placeholder="0" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-success addtolist">Add</button>
            </div>
        </div>
        </div>
    </div>
    </div>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/collectorscript.js') }}"></script>    
@endsection