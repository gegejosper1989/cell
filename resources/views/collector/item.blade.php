@extends('layout.collector')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Items</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/collector/dashboard">Home</a></li>
              <li class="breadcrumb-item active">Items</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    <div class="card card-solid">
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-sm-6">
                
                <div class="col-12">
                <?php $count = 0; ?>
                    @foreach($data_item->pic as $Pic)
                        <?php 
                            if($count < 1){
                        ?>
                        <img src="{{asset('productimg')}}/{{$Pic->file_name}}" class="product-image">
                        <?php
                            
                            }
                            
                            $count++;
                        ?>
                    @endforeach
                
                </div>
                <div class="col-12 product-image-thumbs">
                    @foreach($data_picture as $Picture)
                        <div class="product-image-thumb">
                            <img src="{{asset('productimg')}}/{{$Picture->file_name}}" alt="{{$data_item->product_name}}">
                        </div>
                    @endforeach
                </div>
                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-check"></i> Success!</h5>
                        {{ Session::get('success') }}
                            @php
                            Session::forget('success');
                            @endphp
                        </div>
                    @endif

                </div>
                
                <div class="col-12 col-sm-6">
                    <h3 class="my-3"> 
                    {{$data_item->product_name}}
                    </h3>
                    <hr>
                
                    <ul class="list-group list-group-unbordered mb-3">
                    <li class="list-group-item">
                        <b>Brand</b>
                        {{$data_item->branddetails->brand_name}}
                    </li>
                    <li class="list-group-item">
                        <b>Model</b> 
                        {{$data_item->model}}    
 
                    </li>
                    <li class="list-group-item">
                        <b>Quantity</b> 
                        {{$data_item->quantity}}    

                    </li>
                    <li class="list-group-item">
                        <b>Installment Amount</b> 
                        {{$data_item->unit_price}}   

                    </li>
                    <li class="list-group-item">
                        <b>Cash Amount</b> 
                        {{$data_item->cash_price}}   
                    </li>
                    </ul>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Credit List</h3>
                        </div>
                        <div class="card-body" style="display: block;">
                            {{$data_credit->links()}}
                            <table class="table table-striped" id="table">
                                <thead>
                                <tr>
                                    <th> Name</th>
                                              
                                    <th> Amount</th>
                                    <th> Downpayment</th>
                                    <th> Balance </th>
                                    
                                    <th> Status</th>
                                </tr>
                                </thead>
                                <tbody class="creditresult">
                                    @foreach($data_credit as $Credit)
                                    <tr>
                                        <td><a href="/collector/account/{{$Credit->account->id}}">{{$Credit->account->lname}}, {{$Credit->account->fname}}</a></td>
                                        
                                        <td>{{$Credit->amount}}</td>
                                        <td>{{$Credit->downpayment}}</td>
                                        <td>{{$Credit->balance}}</td>
                                        
                                        <td>{{$Credit->status}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Inventory Record</h3>
                        </div>
                        <div class="card-body" style="display: block;">
                            <table class="table table-striped" id="table">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th> Batch ID </th>             
                                    <th> Receive Quantity</th>
                                    <th> </th>
                                </tr>
                                </thead>
                                <tbody class="productresult">
                                @foreach($data_stock_detail as $stock_detail)
                                <tr>
                                    <td>{{$stock_detail->stock->date_receive}}</td>
                                    <td>
                                        {{$stock_detail->batch_id}}
                                    </td>
                                    
                                    <td>
                                        + {{$stock_detail->quantity_receive}}
                                    </td>
                                    <td>
                                        @if($stock_detail->status != 'RECEIVE')
                                        <a href="javascript:;" class="btn btn-success btn-sm no-print addstockmodal"
                                                data-product_name="{{$stock_detail->product->product_name}}" 
                                                data-product_id="{{$stock_detail->product->id}}" 
                                                data-stock_detail_id="{{$stock_detail->id}}" 
                                                data-model="{{$stock_detail->product->model}}"  
                                                >
                                        <i class="fas fa-plus"></i>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
      </div>
    </section>

    <script>
		$(document).ready(function () {
	            $.ajaxSetup({
	                headers: {
	                    'X-CSRF-TOKEN': '{{csrf_token()}}'
	                }
	            });

	            $('.xedit').editable({
	                url: '{{url("/collector/item/edit")}}',
	                title: 'Update',
	                success: function (response, newValue) {
	                    console.log('Updated', response)
	                }
                });
        })
    </script>

    
@endsection