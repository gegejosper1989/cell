$(document).ready(function() {

    $(document).on('click', '.paymentmodal', function() {
          
          $('#fid').val($(this).data('id'));
          $('#bill_id').val($(this).data('billid'));
          $('#account_id').val($(this).data('acountid'));
          $('#credit_id').val($(this).data('creditid'));
          $('#amount_to_pay').val($(this).data('amountopay'));
          $('#balance').val($(this).data('balance'));
          $('#modalpayment').modal('show');
    });

    $(document).on('click', '.cancelmodal', function() {
        $('#credit_id').val($(this).data('credit_id'));
        $('#modalcancel').modal('show');
  });

    $('.modal-footer').on('click', '.processpayment', function() {
  
        $.ajax({
            type: 'post',
            url: '/collector/bill/pay',
            data: {
                //_token:$(this).data('token'),
                '_token': $('input[name=_token]').val(),
                'bill_id': $("#bill_id").val(),
                'credit_id': $("#credit_id").val(),
                'account_id': $("#account_id").val(),
                'amount': $('#amount').val(),
                'balance': $('#balance').val()
            },
            success: function(data) {
              alert('Payment Success');
              location.reload();
             
              }
        });
    });

    $(document).on('click', '.addstockmodal', function() {
        $('#item').val($(this).data('product_name'));
        $('#stock_detail_id').val($(this).data('stock_detail_id'));
        $('#product_id').val($(this).data('product_id'));
        $('#model').val($(this).data('model'));
        $('#modalstock').modal('show');
  });

  $('.modal-footer').on('click', '.addtolist', function() {

      $.ajax({
          type: 'post',
          url: '/collector/stock/update_stock_record',
          data: {
              //_token:$(this).data('token'),
              '_token': $('input[name=_token]').val(),
              'product_id': $("#product_id").val(),
              'stock_detail_id': $("#stock_detail_id").val(),
              'quantity': $("#quantity").val()
          },
          success: function(data) {
            location.reload();
           
            }
      });
  });

});
  