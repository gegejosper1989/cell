$(document).ready(function() {

    $(document).on('click', '.paymentmodal', function() {
          
          $('#fid').val($(this).data('id'));
          $('#bill_id').val($(this).data('billid'));
          $('#account_id').val($(this).data('acountid'));
          $('#credit_id').val($(this).data('creditid'));
          $('#amount_to_pay').val($(this).data('amountopay'));
          $('#balance').val($(this).data('balance'));
          $('#modalpayment').modal('show');
    });

    $(document).on('click', '.cancelmodal', function() {
        $('#credit_id').val($(this).data('credit_id'));
        $('#modalcancel').modal('show');
    });

    $(document).on('click', '.cancelpaymentmodal', function() {
        $('#payment_id').val($(this).data('payment_id'));
        $('#bill_id').val($(this).data('bill_id'));
        $('#modalcancelpayment').modal('show');
    });

    $('.modal-footer').on('click', '.processpayment', function() {
  
        $.ajax({
            type: 'post',
            url: '/admin/bill/pay',
            data: {
                //_token:$(this).data('token'),
                '_token': $('input[name=_token]').val(),
                'bill_id': $("#bill_id").val(),
                'credit_id': $("#credit_id").val(),
                'account_id': $("#account_id").val(),
                'amount': $('#amount').val(),
                'balance': $('#balance').val()
            },
            success: function(data) {
              alert('Payment successfully recorded!');
              location.reload();
             
              }
        });
    });

    $('.modal-footer').on('click', '.cancelcredit', function() {
  
        $.ajax({
            type: 'post',
            url: '/admin/credit/cancel',
            data: {
                //_token:$(this).data('token'),
                '_token': $('input[name=_token]').val(),
                'bill_id': $("#bill_id").val(),
                'credit_id': $("#credit_id").val(),
                'account_id': $("#account_id").val(),
                'amount': $('#amount').val(),
                'balance': $('#balance').val()
            },
            success: function(data) {
              alert('Credit successfully canceled');
              location.reload();
              }
        });
    });

    $('.modal-footer').on('click', '.cancelpayment', function() {
  
        $.ajax({
            type: 'post',
            url: '/admin/payment/cancel',
            data: {
                //_token:$(this).data('token'),
                '_token': $('input[name=_token]').val(),
                'payment_id': $("#payment_id").val(),
                'bill_id': $("#bill_id").val()
            },
            success: function(data) {
              alert('Payment successfully canceled!');
              location.reload();
              }
        });
    });

    

});
  