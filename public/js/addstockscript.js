$(document).ready(function() {

    $(document).on('click', '.addstockmodal', function() {
          $('#item').val($(this).data('product_name'));
          $('#stock_detail_id').val($(this).data('stock_detail_id'));
          $('#product_id').val($(this).data('product_id'));
          $('#model').val($(this).data('model'));
          $('#modalstock').modal('show');
    });

    $('.modal-footer').on('click', '.addtolist', function() {
  
        $.ajax({
            type: 'post',
            url: '/admin/stock/update_stock_record',
            data: {
                //_token:$(this).data('token'),
                '_token': $('input[name=_token]').val(),
                'product_id': $("#product_id").val(),
                'stock_detail_id': $("#stock_detail_id").val(),
                'quantity': $("#quantity").val()
            },
            success: function(data) {
              location.reload();
             
              }
        });
    });

});
  