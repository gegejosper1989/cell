<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\Brand;
use App\Product;
use Carbon\Carbon;
use App\Credit;
use App\Bill;
use App\Payment;
use App\User;
class AdminController extends Controller
{
    //
    public function index(){
        $today = date('Y-m-d'); 
        $data_account = Account::latest()->get();
        $data_payment = Payment::sum('amount');
        $data_bill_due = Bill::where('due_date', '<=', $today)->get(); 
        $data_bill_collectibles = Bill::where('status', '=', 'not paid')->get(); 
        //dd($data_bill_due);
        //$startDate = Carbon::parse($req->fromdate.' 00:00:00');
        $collectibles = 0;
        foreach($data_bill_collectibles as $collectible){
            $collectibles = $collectibles +$collectible->balance;
        }
        $data_brand = Brand::get();
       
        $brands = [];
        foreach($data_brand as $brand){
            $array_brand = ['value' => $brand->id, 'text' => $brand->brand_name];
            array_push($brands, $array_brand);
        }
        $data_item = Product::with('pic', 'branddetails')->where('status', '=', 'active')->get();
        return view('admin.dashboard', compact('data_bill_due', 'collectibles', 'data_account', 'data_payment', 'data_brand', 'brands', 'data_item'));
    }

    public function credit(){

        $data_brand = Brand::get();
        $data_account = Account::latest()->get();
        $brands = [];
        foreach($data_brand as $brand){
            $array_brand = ['value' => $brand->id, 'text' => $brand->brand_name];
            array_push($brands, $array_brand);
        }
        $data_credit = Credit::with('account', 'product', 'payment_record')->where('status', '=', 'not paid')->paginate(50);
        $data_item = Product::with('pic', 'branddetails')->where('status', '=', 'active')->get();
        //dd($data_credit);
        return view('admin.credit', compact('data_brand', 'data_item', 'brands', 'data_credit', 'data_account'));
    }

    public function credit_all(){
        $data_credit = Credit::with('account', 'product', 'payment_record')->where('status', '=', 'not paid')->get();
        
        return view('admin.credit-view-all', compact('data_credit'));
    }

    public function accounts(){
        $data_account = Account::latest()->paginate(50);
        return view('admin.accounts', compact('data_account'));
    }
    

    
    public function payments(){

        $from_date = date("Y-m-d"); 
        $to_date = date("Y-m-d"); 
        
        $startDate = Carbon::parse($from_date.' 00:00:00');
        $endDate = Carbon::parse($to_date .' 23:59:59'); 
        $data_payment = Payment::whereBetween('created_at', [$startDate, $endDate])->with('bill.credit.product', 'user', 'account')->latest()->paginate(50);
        //dd($data_payment);
        return view('admin.payments', compact('data_payment', 'from_date', 'to_date'));
    }

    public function filter_payments(Request $req){

        $org_from_date = $req->fromdate;  
        $new_from_date = date("Y-m-d", strtotime($org_from_date)); 
        
        $org_to_date = $req->todate;  
        $new_to_date = date("Y-m-d", strtotime($org_to_date)); 
        $from_date = $org_from_date; 
        $to_date = $org_to_date; 
        $startDate = Carbon::parse($new_from_date.' 00:00:00');
        $endDate = Carbon::parse($new_to_date .' 23:59:59'); 
        $data_payment = Payment::whereBetween('created_at', [$startDate, $endDate])->with('bill.credit.product', 'user', 'account')->latest()->paginate(50);
        //dd($data_payment);
        return view('admin.payments', compact('data_payment', 'from_date', 'to_date'));
    }

    public function reports(){
        
        return view('admin.reports');
    }

    public function settings(){
        
        return view('admin.settings');
    }

    public function items(){
        
        $data_brand = Brand::get();
        $brands = [];
        foreach($data_brand as $brand){
            $array_brand = ['value' => $brand->id, 'text' => $brand->brand_name];
            array_push($brands, $array_brand);
        }
        $data_item = Product::with('pic', 'branddetails')->orderBy('status', 'asc')->paginate(50);
        //dd($data_item);
        return view('admin.items', compact('data_brand', 'data_item', 'brands'));
    }

    public function users(){
        $data_user = User::latest()->paginate(50);
        return view('admin.users', compact('data_user'));
    }


    
}
