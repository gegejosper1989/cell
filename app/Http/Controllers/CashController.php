<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cash;
use App\Product;

class CashController extends Controller
{
    //
    public function add_cash(Request $req){
        $data_cash = new Cash();
        $data_cash->product_id = $req->cash_product_id;
        $data_cash->name = strtoupper($req->name);
        $data_cash->quantity = $req->cash_quantity;
        $data_cash->amount = $req->cash_amount;
        $data_cash->status = 'active';
        $data_cash->save();

        $data_product = Product::where('id', '=', $req->cash_product_id)->first();
        $new_product_quantity = $data_product->quantity - $req->cash_quantity;

        Product::find($req->cash_product_id)->update(['quantity' => $new_product_quantity]);
        return redirect('/admin/cash/view/'.$data_cash->id)->with('success','Cash item successfully process!');     
    }

    public function cash_view($cash_id){
        $data_cash = Cash::with('product')->where('id', '=', $cash_id)->first();
        
        return view('admin.cash-view', compact('data_cash'));
    }
    public function cash(){
        $data_cash = Cash::with('product')->latest()->paginate(100);
        $data_item = Product::with('pic', 'branddetails')->where('status', '=', 'active')->get();
        return view('admin.cash', compact('data_cash', 'data_item'));
    }
}
