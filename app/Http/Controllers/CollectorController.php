<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\Product;
use Carbon\Carbon;
use App\Credit;
use App\Bill;
use App\Payment;
use App\User;

class CollectorController extends Controller
{
    //
    public function index(){
        $today = date('Y-m-d'); 
        $data_account = Account::latest()->get();
        $data_payment = Payment::sum('amount');
        $data_bill_due = Bill::where('due_date', '<=', $today)->get(); 
        $data_bill_collectibles = Bill::where('status', '=', 'not paid')->get(); 
        //dd($data_bill_due);
        //$startDate = Carbon::parse($req->fromdate.' 00:00:00');
        $collectibles = 0;
        foreach($data_bill_collectibles as $collectible){
            $collectibles = $collectibles +$collectible->balance;
        }
        return view('collector.dashboard', compact('data_bill_due', 'collectibles', 'data_account', 'data_payment'));
    }

    public function credit(){


        $data_account = Account::latest()->get();

        $data_credit = Credit::with('account', 'product', 'payment_record')->where('status', '=', 'not paid')->paginate(50);
        $data_item = Product::with('pic', 'branddetails')->where('status', '=', 'active')->get();
        //dd($data_credit);
        return view('collector.credit', compact('data_item', 'brands', 'data_credit', 'data_account'));
    }
    public function credit_all(){
        $data_credit = Credit::with('account', 'product', 'payment_record')->where('status', '=', 'not paid')->get();
        
        return view('collector.credit-view-all', compact('data_credit'));
    }

    public function payments(){
        $data_payment = Payment::with('bill.credit.product', 'user', 'account')->latest()->paginate(50);
        //dd($data_payment);
        return view('collector.payments', compact('data_payment'));
    }

    public function items(){

        $data_item = Product::with('pic', 'branddetails')->orderBy('status', 'asc')->paginate(50);
        //dd($data_item);
        return view('collector.items', compact('data_item'));
    }
    public function accounts_controller(){
        $data_account = Account::latest()->paginate(50);
        return view('collector.accounts', compact('data_account'));
    }
}
