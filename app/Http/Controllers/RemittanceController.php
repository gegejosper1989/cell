<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Remittance;

class RemittanceController extends Controller
{
    //
    public function remittance(){
        $data_remittance = Remittance::latest()->paginate(50);
        return view('admin.remittance', compact('data_remittance'));
    }

    public function add_remittance(Request $req){
        $data_remittance = new Remittance();
        $data_remittance->remittance_date = date('d/m/Y');
        $data_remittance->inclusion_date = $req->inclusion_date;
        $data_remittance->amount_remittance = $req->amount_remittance;
        $data_remittance->status = 'active';
        $data_remittance->save();

        return redirect('/admin/remittance')->with('success','Remittance succesfully recorded!');     
    }
}
