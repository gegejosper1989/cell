<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stock;
use App\Stockdetail;
use App\Product;

class StockController extends Controller
{
    //
    public function stocks(){
        $data_stock = Stock::orderBy('status', 'desc')->paginate(50);
        $data_stock_id = Stock::count();
        return view('admin.stocks', compact('data_stock', 'data_stock_id'));
    }

    public function stocks_controller(){
        $data_stock = Stock::orderBy('status', 'desc')->where('status', '!=', 'PENDING')->paginate(50);
        $data_stock_id = Stock::count();
        return view('collector.stocks', compact('data_stock', 'data_stock_id'));
    }

    

    public function add_stock(Request $req){
        //dd($req);
        $data_stock = new Stock();
        $data_stock->batch_id = $req->batch_number;
        $data_stock->date_send = $req->date_send;
        $data_stock->date_receive = 'null';
        $data_stock->status = 'PENDING';
        $data_stock->save();

        return redirect('/admin/stock/add_stock/'.$data_stock->id)->with('success','Item successfully added!');
    }

    public function add_stocks_record($stock_id){
        $data_stock = Stock::where('id', '=', $stock_id)->first();
        $data_stock_detail = Stockdetail::where('batch_id', '=', $data_stock->batch_id)->get();
        $data_item = Product::with('pic', 'branddetails')->get();
        return view('admin.stock-add-record', compact('data_stock', 'data_stock_detail', 'data_item'));
    }

    public function add_stocks_record_detail(Request $req){
        $data_stock_detail = new Stockdetail();
        $data_stock_detail->batch_id = $req->batch_id;
        $data_stock_detail->product_id = $req->product_id;
        $data_stock_detail->quantity = $req->quantity;
        $data_stock_detail->quantity_receive = 0;
        $data_stock_detail->status = 'PENDING';
        $data_stock_detail->save();

        return($data_stock_detail);
    }

    public function update_stock_record(Request $req){
        $update_stock_detail = Stockdetail::where('id', '=', $req->stock_detail_id)
        ->update(['quantity_receive' => $req->quantity]);

       
        return($update_stock_detail);
    }

    public function update_stock_record_collector(Request $req){
        $update_stock_detail = Stockdetail::where('id', '=', $req->stock_detail_id)
        ->update(['quantity_receive' => $req->quantity]);
        //dd($update_stock_detail);
        return($update_stock_detail);
    }

    public function remove_stocks_record($stock_detail_id){
        $delete_stock_detail = Stockdetail::find($stock_detail_id)
                    ->delete();
        return redirect()->back()->with('danger','Item successfully remove!');
    }

    public function save_stock($stock_batch_id){
        $update_stock_detail = Stockdetail::where('batch_id', '=', $stock_batch_id)
                    ->update(['status' => 'SENDING']);
        $update_stock = Stock::where('batch_id', '=', $stock_batch_id)
                    ->update(['status' => 'SENDING']);
        return redirect('/admin/stock/view/'.$stock_batch_id)->with('success','Stock successfully processed!');
    }

    public function receive_stock($stock_batch_id){

        $data_stock_detail = Stockdetail::where('batch_id', '=', $stock_batch_id)->get();
        foreach($data_stock_detail as $stock_detail){
            $data_stock = Product::where('id', '=', $stock_detail->product_id)->first();
            $new_stock = $data_stock->quantity + $stock_detail->quantity_receive;
            $update_stock = Product::where('id', '=', $stock_detail->product_id)
            ->update(['quantity' => $new_stock]);
        }

        $update_stock_detail = Stockdetail::where('batch_id', '=', $stock_batch_id)
                    ->update(['status' => 'RECEIVED']);
        $update_stock = Stock::where('batch_id', '=', $stock_batch_id)
                    ->update(['status' => 'RECEIVED', 'date_receive' => date('Y-m-d')]);
        return redirect('/admin/stock/view/'.$stock_batch_id)->with('success','Stock successfully received!');
    }

    public function receive_stock_collector($stock_batch_id){

        $data_stock_detail = Stockdetail::where('batch_id', '=', $stock_batch_id)->get();
        foreach($data_stock_detail as $stock_detail){
            $data_stock = Product::where('id', '=', $stock_detail->product_id)->first();
            $new_stock = $data_stock->quantity + $stock_detail->quantity_receive;
            $update_stock = Product::where('id', '=', $stock_detail->product_id)
            ->update(['quantity' => $new_stock]);
        }

        $update_stock_detail = Stockdetail::where('batch_id', '=', $stock_batch_id)
                    ->update(['status' => 'RECEIVED']);
        $update_stock = Stock::where('batch_id', '=', $stock_batch_id)
                    ->update(['status' => 'RECEIVED', 'date_receive' => date('Y-m-d')]);
        return redirect('/collector/stock/view/'.$stock_batch_id)->with('success','Stock successfully received!');
    }

    

    

    public function view_stock($stock_batch_id){
        $data_stock = Stock::where('batch_id', '=', $stock_batch_id)->first();
        $data_stock_detail = Stockdetail::with('product')->where('batch_id', '=', $stock_batch_id)->get();
        return view('admin.stock-view-record', compact('data_stock', 'data_stock_detail'));
    }

    public function view_stock_collector($stock_batch_id){
        $data_stock = Stock::where('batch_id', '=', $stock_batch_id)->first();
        $data_stock_detail = Stockdetail::with('product')->where('batch_id', '=', $stock_batch_id)->get();
        return view('collector.stock-view-record', compact('data_stock', 'data_stock_detail'));
    }
}
