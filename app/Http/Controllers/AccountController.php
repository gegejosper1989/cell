<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\Product;
use App\User;
use Illuminate\Support\Facades\Hash;
use Validator;
use Response;
use DB;
use App\Payment;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    //
    public function add_account(Request $req){
        
        $rules = array(
            'fname' => 'required|string|max:255',
            'lname' => 'required|string|max:255',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator->getMessageBag());
        }
        else {
            $count_account = Account::count();
            if($count_account ==0){
                $account_id = 1;
            }
            else {
                $latest_account = Account::latest()->first();
                $account_id = $latest_account->id + 1;
            }

            //dd($latest_account);
            $data_user = new User();
            $data_user->name = strtoupper($req['lname'].' '.$req['fname'].' '.$req['mname']) ;
            $data_user->username = 'account'.$account_id;
            $data_user->email = 'account'.$account_id.'@qcellshop.com';
            $data_user->password = Hash::make('password');
            $data_user->usertype = 'account';
            $data_user->status = 'active';
            $data_user->save();

            $data_account = new Account();
            $data_account->user_id = $data_user->id;
            $data_account->fname = $req->fname;
            $data_account->lname = $req->lname;
            $data_account->address = $req->address;
            $data_account->contact_number = $req->contact_num;
            $data_account->fb_link = $req->fb_link;
            $data_account->status = 'active';
            $data_account->save();
        }
        return redirect()->back()->with('success','Account successfully added!');
    }
    public function edit_account(Request $request){

        if($request->ajax()){
            Account::find($request->input('pk'))->update([$request->input('name') => $request->input('value')]);
            dd($request);
            
            return response()->json(['success' => true]);
        }
    }
    public function account(){
        if (Auth::check()){
            $user_id = Auth::user()->id;
        }
        $data_account = Account::with('credit.product', 'bill.credit')->where('user_id', '=', $user_id)->first();
        //dd($data_account->bill);
        $data_payment = Payment::where('account_id', '=', $user_id)->with('bill.credit.product', 'user')->take(10)->get();
        //dd($data_payment);
        return view('account.dashboard', compact('data_account', 'data_payment', 'user_id'));

    }
    public function bill_history($account_id){
        if (Auth::check()){
            $user_id = Auth::user()->id;
        }
        $data_account = Account::with('credit.product', 'bill.credit')->where('id', '=', $account_id)->first();
        //dd($data_account->bill);
        
        return view('account.account-bill', compact('data_account', 'user_id'));

    }
    public function payment_history($account_id){
        if (Auth::check()){
            $user_id = Auth::user()->id;
        }
        $data_account = Account::with('credit.product', 'bill.credit')->where('id', '=', $account_id)->first();
        //dd($data_account->bill);
        $data_payment = Payment::where('account_id', '=', $account_id)->with('bill.credit.product', 'user')->get();
        //dd($data_payment);
        return view('account.account-payment', compact('data_account', 'data_payment', 'user_id'));

    }

    public function items(){
        if (Auth::check()){
            $user_id = Auth::user()->id;
        }
        $data_item = Product::with('pic', 'branddetails')->orderBy('status', 'asc')->paginate(50);
        return view('account.items', compact('data_item', 'user_id'));
    }

    public function view_account($account_id){
        if (Auth::check())
        {
            $user_id = Auth::user()->id;
        }
        $data_account = Account::with('credit.product', 'bill.credit')->where('id', '=', $account_id)->first();
        //dd($data_account->bill);
        $data_payment = Payment::where('account_id', '=', $account_id)->with('bill.credit.product', 'user')->take(10)->get();
        //dd($data_payment);
        return view('admin.account', compact('data_account', 'data_payment'));

    }
    public function view_account_controller($account_id){
        $data_account = Account::with('credit.product', 'bill.credit')->where('id', '=', $account_id)->first();
        //dd($data_account->bill);
        $data_payment = Payment::where('account_id', '=', $account_id)->with('bill.credit.product', 'user')->take(10)->get();
        //dd($data_payment);
        return view('collector.account', compact('data_account', 'data_payment'));

    }
    public function view_account_bill_history($account_id){
        $data_account = Account::with('credit.product', 'bill.credit')->where('id', '=', $account_id)->first();
        //dd($data_account->bill);
        
        return view('admin.account-bill', compact('data_account'));

    }

    public function view_account_bill_history_controller($account_id){
        $data_account = Account::with('credit.product', 'bill.credit')->where('id', '=', $account_id)->first();
        //dd($data_account->bill);
        
        return view('collector.account-bill', compact('data_account'));

    }

    public function view_account_payment_history($account_id){
        $data_account = Account::with('credit.product', 'bill.credit')->where('id', '=', $account_id)->first();
        //dd($data_account->bill);
        $data_payment = Payment::where('account_id', '=', $account_id)->with('bill.credit.product', 'user')->get();
        //dd($data_payment);
        return view('admin.account-payment', compact('data_account', 'data_payment'));

    }
    public function view_account_payment_history_controller($account_id){
        $data_account = Account::with('credit.product', 'bill.credit')->where('id', '=', $account_id)->first();
        //dd($data_account->bill);
        $data_payment = Payment::where('account_id', '=', $account_id)->with('bill.credit.product', 'user')->get();
        //dd($data_payment);
        return view('collector.account-payment', compact('data_account', 'data_payment'));

    }

    

    public function account_search(Request $request){
        if($request->ajax())
        {
            $search = $request->search;
            $output="";
            $data_account = DB::table('accounts')
                ->where(function($query) use ($search){
                    $query->where('accounts.fname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('accounts.lname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('accounts.mname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('accounts.contact_number', 'LIKE', '%'.$search.'%');
                })
                ->latest()
                ->get();
            if($data_account)
            {
                $count=1;
                foreach ($data_account as $Account) {
                    $output.='<tr class="item'.$Account->id.'">
                        <td><a href="/admin/account/'.$Account->id.'">'.$Account->id.'</a></td>
                        <td><a href="/admin/account/'.$Account->id.'">'.strtoupper($Account->lname).', '.strtoupper($Account->fname).' '.strtoupper($Account->mname).'</td>
                        <td>'.$Account->address.'</td>
                        <td>'.$Account->contact_number.'</td>
                        <td><a href="/admin/account/'.$Account->id.'" class="btn btn-info btn-small"><i class="fa fa-search"></i></a></td>';
                    $output .='</tr>';
                    $count++;
                }
                return Response($output);
            }
        }
    }
    public function account_search_collector(Request $request){
        if($request->ajax())
        {
            $search = $request->search;
            $output="";
            $data_account = DB::table('accounts')
                ->where(function($query) use ($search){
                    $query->where('accounts.fname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('accounts.lname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('accounts.mname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('accounts.contact_number', 'LIKE', '%'.$search.'%');
                })
                ->latest()
                ->get();
            if($data_account)
            {
                $count=1;
                foreach ($data_account as $Account) {
                    $output.='<tr class="item'.$Account->id.'">
                        <td><a href="/collector/account/'.$Account->id.'">'.$Account->id.'</a></td>
                        <td><a href="/collector/account/'.$Account->id.'">'.strtoupper($Account->lname).', '.strtoupper($Account->fname).' '.strtoupper($Account->mname).'</td>
                        <td>'.$Account->address.'</td>
                        <td>'.$Account->contact_number.'</td>
                        <td><a href="/collector/account/'.$Account->id.'" class="btn btn-info btn-small"><i class="fa fa-search"></i></a></td>';
                    $output .='</tr>';
                    $count++;
                }
                return Response($output);
            }
        }
    }
}
