<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Productpic;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        $data_item = Product::where('status', '=', 'active')->with('pic', 'branddetails')->latest()->paginate(50);
    
        return view('welcome', compact('data_item'));
    }

    public function log_in(){
        return view('login');
    }

    public function view_product($item_id){

       
        $data_picture = Productpic::where('product_id', '=', $item_id)->get();
        $data_item = Product::where('id', '=', $item_id)
            ->with('pic', 'branddetails')
            ->first();
        
        return view('product', compact('data_item', 'data_picture'));
    }

}
