<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    //
    public function stockdetail()
    {
        return $this->hasMany('App\Stockdetail','batch_id','batch_id');
    }
}
