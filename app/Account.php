<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    //
    protected $fillable = array('fname', 'lname', 'fb_link', 'contact_number', 'address', 'mname');
    public function credit()
    {
        return $this->hasMany('App\Credit','account_id');
    }

    public function bill()
    {
        return $this->hasMany('App\Bill','account_id','id');
    }
}
