<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stockdetail extends Model
{
    //
    public function product()
    {
        return $this->belongsTo('App\Product','product_id','id');
    }
    public function stock()
    {
        return $this->belongsTo('App\Stock','batch_id','batch_id');
    }
}
